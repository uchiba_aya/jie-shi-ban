<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
    </head>
    <body>
        <div class="main-contents">

			<div class="header">
			    <c:if test="${ empty loginUser }">
			        <a href="login">ログイン</a>
			    </c:if>
			    <c:if test="${ not empty loginUser }">
			        <a href="./">ホーム</a>
			        <a href="settings">設定</a>
			        <a href="logout">ログアウト</a>
			    </c:if>
			</div>
			<div class="form-area">
			    <c:if test="${ isShowMessageForm }">
			        <form action="newMessage" method="post">
			            いま、どうしてる？<br />
			            <textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
			            <br />
			            <input type="submit" value="つぶやく">
			        </form>
			    </c:if>
			</div>
			<div class="header">
                <a href="newtext">投稿する</a>
            </div>
            <c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			        <div class="login_id">
			            @<c:out value="${loginUser.login_id}" />
			        </div>

			    </div>
			</c:if>
			<div class="header">
                <a href="usercontrol">ユーザー管理</a>
            </div>
            <c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			        <div class="login_id">
			            @<c:out value="${loginUser.login_id}" />
			        </div>

			    </div>
			</c:if>

            <div class="copyright"> Copyright(c)Uchiba Aya</div>
        </div>
    </body>
</html>