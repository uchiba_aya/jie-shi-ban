package beans;

import java.io.Serializable;
import java.util.Date;

public class NewText implements Serializable {
    private static final long serialVersionUID = 1L;

    private String subject;  //subject 件名
    private String text;  //text 本文
    private String category;  //category カテゴリー
    private Date created_at;  //created_at 登録日時
    private String registrant;  //registrant 登録者

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String category() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreatedDate() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public String getRegistrant() {
		return registrant;
	}
	public void setRegistrant(String registrant) {
		this.registrant = registrant;
	}

}