package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewText;
import beans.User;
import service.NewTextService;

@WebServlet(urlPatterns = { "/newText" })
public class NewTextServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            NewText newtext = new NewText();
            newtext.setText(request.getParameter("newtext"));
            newtext.setRegistrant(user.getLogin_id());

            new NewTextService().register(newtext);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String NewText = request.getParameter("newtext");

        if (StringUtils.isEmpty(NewText) == true) {
            messages.add("メッセージを入力してください");
        }
        if (1000 < NewText.length()) {
            messages.add("1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}