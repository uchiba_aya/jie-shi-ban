package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.NewText;
import dao.NewTextDao;

public class NewTextService {

    public void register(NewText newtext) {

        Connection connection = null;
        try {
            connection = getConnection();

            NewTextDao newtextDao = new NewTextDao();
            newtextDao.insert(connection, newtext);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}